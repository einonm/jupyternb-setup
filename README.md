# Jupyter notebook setup

Various scripts to install, launch and use Jupyter Notebooks on gnome3 / SGE / PBS / slurm


## Installation and usage

Requires at least Anaconda3 5.3.0 to be installed (https://www.anaconda.com/)

Launchers install bash and python3 kernels by default.

install-irkernel.sh installs an R kernel.

## Contributing
Contributing issues, feature proposals or code is actively welcomed - please see the [CONTRIBUTING.md](CONTRIBUTING.md) file for more details.

## Code of Conduct
We want to create a welcoming environment for everyone who is interested in contributing. Please see the [Contributor Covenant Code of Conduct](CODE_OF_CONDUCT.md) file to learn more about our commitment to an open and welcoming environment.

## Copyright and Licence Information

Copyright Cardiff University 2022.
For licensing information, see the [LICENSE file](LICENSE).

