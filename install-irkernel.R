# set a default repository for packages
local({r <- getOption("repos")
       r["CRAN"] <- "http://www.stats.bris.ac.uk/R/"
       options(repos=r)
})

if (!require(pbdZMQ)) {
    install.packages('pbdZMQ', dependencies=T)
}

if (!require(repr)) {
    install.packages('repr', dependencies=T)
}

if (!require(devtools)) {
    install.packages('devtools', dependencies=T)
}

devtools::install_github('IRkernel/IRkernel')
IRkernel::installspec(user=T)

