#! /usr/bin/env python3

# run_slurm_notebook_server.py - Start a jupyter notebook server
#                                on a slurm cluster allocation
#
# Copyright (C) 2020 Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import pexpect as pe
import sys
import argparse
import socket

def check_port(port):
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex(('localhost', port)) == 0


parser = argparse.ArgumentParser(description='Allocate resources and start a Jupyter Notebook server on a remote slurm cluster.')
parser.add_argument('-s', '--ssh', dest='sshalias', required=True,
                    help='A valid ssh alias (defined in ~/.ssh/config) for a slurm cluster that uses an ssh key')
parser.add_argument('-p', '--partition', dest='partition', required=True,
                    help='A slurm partition (queue) from which to request resources')
parser.add_argument('-a', '--account', dest='account', required=True,
                    help='A slurm project account ID')
parser.add_argument('-m', '--mem', dest='memory', required=False,
                    help='A memory allocation amount for the server, in size[units]. Default unit is M (MB), default value is 9.5G')
parser.add_argument('-b', '--browser', dest='browser', required=False,
                    help='Specify the browser program to use, default is "brave-browser"')
parser.add_argument('-n', '--headnode', dest='headnode', required=False, action='store_true',
                    help='Start server on the headnode, e.g. to support internet access')

a = parser.parse_args()

localport=8899
while check_port(localport):
    localport = localport - 1

if a.memory:
    memory = a.memory
else:
    # This matches the default on Hawk c_compute_neuro1
    memory = '9550M'

if a.browser:
    browser = a.browser
else:
    browser = "brave-browser"

# Get job on hawk cluster to run notebook
if a.headnode:
    print("****Running command: ssh " + a.sshalias + " hostname")
    alloc = pe.spawnu("ssh " + a.sshalias + " hostname")
    alloc.expect('\r\n')
    node = alloc.before
    print ("Allocating resources on head node " + node)
else:
    print ("Allocating resources on " + a.sshalias + " partition " + a.partition + " for account " + a.account + "...")
    print("****Running command: ssh " + a.sshalias + " salloc -p " + a.partition + " --account=" + a.account + " --mem=" + memory)
    alloc = pe.spawnu("ssh " + a.sshalias + " salloc -p " + a.partition + " --account=" + a.account + " --mem=" + memory)
    alloc.expect("salloc: Nodes (.*) are ready for job")
    node = alloc.match.group(1)

# Run notebook on node - assumes here the script is in the home dir
print ("Starting the notebook server...")
print("****Running command: ssh " + a.sshalias + " ssh " + node + " ./run_jupyter_notebook_server.sh")
jupyter = pe.spawnu("ssh " + a.sshalias + " ssh " + node + " ./run_jupyter_notebook_server.sh")
jupyter.expect("or http://127.0.0.1:([0-9]+)/\?token=([0-9a-f]+)\s")
jupyter_port = jupyter.match.group(1)
jupyter_token = jupyter.match.group(2)

# Get IP of node
print ("getting IP of node " + node)
print("****Running command: ssh " + a.sshalias + " ping " + node)
ip = pe.spawnu("ssh " + a.sshalias + " ping " + node)
ip.expect("bytes from " + node + " \((.*)\).*")
ip_addr = ip.match.group(1)

# Start ssh tunnel
print ("Setting up ssh tunnelling...")
print("****Running command: ssh -L " + str(localport) + ":" + ip_addr + ":" + jupyter_port + " " + a.sshalias)
tunnel = pe.spawnu("ssh -L " + str(localport) + ":" + ip_addr + ":" + jupyter_port + " " + a.sshalias)
tunnel.expect("\$")
print ("Running a Jupyter Notebook server on " + a.sshalias + " node " + node + "(" + ip_addr + "), port " + jupyter_port)
print ("The access token is: " + jupyter_token)

viewserver = pe.spawnu(browser + " http://localhost:" + str(localport) + "/?token=" + jupyter_token)
print("****Running command: " + browser + " http://localhost:" + str(localport) + "/?token=" + jupyter_token)

input("Press any key to shut down the notebook server")





