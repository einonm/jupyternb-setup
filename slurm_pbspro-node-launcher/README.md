# Slurm setup

* Install pexpect on your local Linux/Mac:

```
$ pip3 install pexpect --user
```
* If using Cardiff University's Hawk Slurm cluster follow the guide at:

  [https://git.cardiff.ac.uk/dpmcn/configure-ssh-agent]

  to start an ssh-agent and generate an id_rsa key, adding the public part to
  your Hawk ~/.ssh/authorized_keys file:

```
~]$ ssh-keygen
Generating public/private rsa key pair.
Enter file in which to save the key (/home/myuser/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /home/myuser/.ssh/id_rsa
Your public key has been saved in /home/myuser/.ssh/id_rsa-.pub
The key fingerprint is:
SHA256:QtMB4w95I6FOvNKYPoqbTMfj3Tp4uYTwC8vQw3CIsDE myuser@mylinux
The key's randomart image is:
+---[RSA 3072]----+
|      +..        |
|   . o = .       |
|E   + B +        |
|o= * o * .       |
|=.= + . S        |
| *+..  .         |
|.o**...          |
|*o=+=+.          |
|== o.++.         |
+----[SHA256]-----+

~]$ ssh-add ~/.ssh/id_rsa
Identity added: /home/myuser/.ssh/id_rsa (/home/myuser/.ssh/id_rsa)

~]$ cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

```

* Copy the run_jupyter_notebook_server.sh script to your Slurm cluster home area

* Run the run_slurm_notebook_server.py on your local Linux or Mac machine, giving
  the required parameters for a slurm partion and account allong with an ssh alias.

Example usage:

```
$ ./run_slurm_notebook_server.py -s hawk -p c_compute_neuro1 -a abc1234
Allocating resources on hawk partition c_compute_neuro1 for account abc1234...
Starting the notebook server...
Setting up ssh tunnelling...
Running a Jupyter Notebook server on hawk node ccs9030(10.222.2.22), port 8888
The access token is: fe44c487560ecf76d084d8f2dc9165f6b77488d7c672e584
Press any key to shut down the notebook server
$_
```

The ssh alias on unix machines can be defined in the file ~/.ssh/config, and would be
similar to this alias for 'hawk':

```
 Host hawk
  	 Hostname hawklogin.cf.ac.uk
 	 User c.username
 	 IdentityFile ~/.ssh/id_rsa-hawk
 	 IdentitiesOnly yes
```
An ssh key (here id_rsa-hawk) set up to access the cluster is required for correct operation
of the slurm script.
