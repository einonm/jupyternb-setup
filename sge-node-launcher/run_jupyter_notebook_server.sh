#! /usr//bin/env bash

# run_jupyter_notebook_server.sh - Start a local/cluster jupyter notebook server
#
# Copyright (C) 2017  Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# Based on guide at:
# https://jupyter.readthedocs.io/en/latest/install.html

source /share/apps/anaconda3/etc/profile.d/conda.sh

# Directory location for local python environment
JDIR=~/jupyter-notebooks

if [ "$1" == "new" ]; then
  conda env remove -p $JDIR
fi

# If the conda environment doesn't exist, create it.
if [ ! -d $JDIR ]; then
  conda create -c r r-essentials numpy pandas -y -p $JDIR
  conda activate $JDIR

  pip install --upgrade pip
  pip install bash_kernel jupyter_client ipython ipykernel jupytext --upgrade
  python -m bash_kernel.install --user

  # enable parallel
  pip install notebook ipyparallel

  # From https://github.com/ipython/ipyparallel/issues/170
  jupyter serverextension enable --py ipyparallel --user
  jupyter nbextension install --py ipyparallel --user
  jupyter nbextension enable --py ipyparallel --user

  # zotero citation extension, https://github.com/takluyver/cite2c
  #pip install cite2c
  #python -m cite2c.install

  jupyter notebook --generate-config -y
  echo "c.NotebookApp.contents_manager_class = \"jupytext.TextFileContentsManager\"" >> ~/.jupyter/jupyter_notebook_config.py
  echo "c.ContentsManager.default_jupytext_formats = \"ipynb,py\"" >> ~/.jupyter/jupyter_notebook_config.py
  # If running on a cluster node, let other IPs see the notebooks
  echo "c.NotebookApp.ip = '0.0.0.0'" >> ~/.jupyter/jupyter_notebook_config.py
else
    conda activate $JDIR
fi

jupyter notebook --NotebookApp.iopub_data_rate_limit=100000000
