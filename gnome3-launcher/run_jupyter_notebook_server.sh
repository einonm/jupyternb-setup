#! /usr//bin/env bash

# run_jupyter_notebook_server.sh - Start a local/cluster jupyter notebook server
#
# Copyright (C) 2017  Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# Based on guide at:
# https://jupyter.readthedocs.io/en/latest/install.html

source /opt/anaconda3/etc/profile.d/conda.sh

# Directory location for local python environment
JDIR=~/jupyter-notebooks

if [ "$1" == "new" ]; then
	conda env remove -p $JDIR
fi

# If the conda environment doesn't exist, create it.
if [ ! -d $JDIR ]; then
  conda create numpy pandas ipykernel jupyter_client ipython nbconvert==5.6.1 -y -p $JDIR
  conda activate $JDIR

  pip install bash_kernel jupytext matplotlib venn matplotlib_venn seaborn statsmodels sklearn --upgrade
  python -m bash_kernel.install --user

  # Spellchecker
  pip install jupyter_contrib_nbextensions
  jupyter contrib nbextension install --user
  jupyter nbextension enable spellchecker/main

  jupyter notebook --generate-config -y
  echo "c.NotebookApp.contents_manager_class = \"jupytext.TextFileContentsManager\"" >> ~/.jupyter/jupyter_notebook_config.py
  echo "c.ContentsManager.default_jupytext_formats = \"ipynb,py\"" >> ~/.jupyter/jupyter_notebook_config.py
else
    conda activate $JDIR
fi

jupyter notebook --NotebookApp.iopub_data_rate_limit=100000000
