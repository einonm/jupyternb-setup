#! /usr/bin/env bash

# Takes one parameter, the notebook file to view

source /opt/anaconda3/etc/profile.d/conda.sh
conda activate ~/jupyter-notebooks

jupyter-nbconvert --to slides $1 --reveal-prefix=reveal.js --post serve
