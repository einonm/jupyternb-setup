#! /bin/bash -x
# install-jupyter-launcher-fedora.sh - Installs files required
# to setup a local jupyter notebook launcher from a gnome3 desktop
#
# Copyright (C) 2017  Mark Einon <mark.einon@gmail.com>
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License,
# Version 2.1 only as published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# Must be run with root privileges
# Assumes that anaconda3 is installed in /opt

# copy icon
sudo cp jupyter_image.png /usr/share/icons/

# copy launcher file
cp jupyter-notebook.desktop ~/.local/share/applications/
sudo chmod +x ~/.local/share/applications/jupyter-notebook.desktop

# copy launcher script
sudo cp run_jupyter_notebook_server.sh /usr/local/bin/
sudo chmod +x /usr/local/bin/run_jupyter_notebook_server.sh



